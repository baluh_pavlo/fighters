export function fight(firstFighter, secondFighter) {

    let health1 = firstFighter.health;
    let health2 = secondFighter.health;
    while (health1 >= 0 || health2 >= 0) {
        health2 -= getDamage(firstFighter, secondFighter);
        if (health2 <= 0) {
            return firstFighter;
        }
        health1 -= getDamage(secondFighter, firstFighter);
        if (health1 <= 0) {
            return secondFighter;
        }
    }


}

export function getDamage(attacker, enemy) {
    let fullDamage = (attacker) - getBlockPower(enemy);
    if (fullDamage <= 0) return 0;
    else return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
    return fighter.attack * Math.floor(Math.random() * 2 + 1);
}

export function getBlockPower(fighter) {
    return fighter.defense * Math.floor(Math.random() * 2 + 1);
}
