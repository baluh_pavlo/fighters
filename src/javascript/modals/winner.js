import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export function showWinnerModal(fighter) {
    const title = "Winner!!!";
    const bodyElement = winnerInfo(fighter);
    showModal({ title, bodyElement });
}

function winnerInfo(fighter) {
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const name = createElement({ tagName: 'div', className: 'fighter-name' });
    const imgElement = createElement({ tagName: 'img', className: 'fighter-img' });
    imgElement.src = fighter.source;
    name.innerText = fighter.name;
    fighterDetails.append(name);
    fighterDetails.append(imgElement);
    return fighterDetails;
}
