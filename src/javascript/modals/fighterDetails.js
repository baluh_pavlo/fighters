import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    console.log(fighter);
    const {name} = fighter;
    const {attack} = fighter;
    const {defense} = fighter;
    const {health} = fighter;
    const {source} = fighter;
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({tagName: 'div', className: 'fighter-name'});
    const attackElement = createElement({tagName: 'div', className: 'fighter-info'});
    const defenceElement = createElement({tagName: 'div', className: 'fighter-info'});
    const healthElement = createElement({tagName: 'div', className: 'fighter-info'});
    const imgElement = createElement({tagName: 'img', className: 'fighter-img'});
    nameElement.innerText = name;
    attackElement.innerText = attack;
    defenceElement.innerText = defense;
    healthElement.innerText = health;
    imgElement.src = source;
    fighterDetails.append(nameElement);
    fighterDetails.append(createElementsForDetails(attackElement, "Fighter attack"));
    fighterDetails.append(createElementsForDetails(defenceElement, "Fighter defence"));
    fighterDetails.append(createElementsForDetails(healthElement, "Fighter health"));
    fighterDetails.append(imgElement);
    return fighterDetails;
}

function createElementsForDetails(el, text) {
    const parameter = createElement({tagName: 'div', className: 'parameter'});
    const parameterName = createElement({tagName: 'div', className: 'parameter-name'});
    parameterName.innerText = text;
    parameter.append(parameterName);
    parameter.append(el);
    return parameter;
}
